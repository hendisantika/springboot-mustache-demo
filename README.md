# Spring Boot Mustache Demo

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-mustache-demo.git`
2. Navigate to the folder: `cd springboot-mustache-demo`
3. Replace MySQL credentials with your own
4. Run the application: `gradle clean bootRun`
5. Open your favorite browser: http://localhost:8080

### Image Screen shots

List All Posts

![List All Posts](img/list1.png "List All Posts")

![List All Posts](img/list2.png "List All Posts")

![List All Posts](img/list3.png "List All Posts")

Add New Post

![Add New Post](img/add1.png "Add New Post")

![Add New Post](img/add2.png "Add New Post")

Edit Post

![Edit Post](img/edit.png "Edit Post")
