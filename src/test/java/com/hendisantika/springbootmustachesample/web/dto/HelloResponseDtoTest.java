package com.hendisantika.springbootmustachesample.web.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/03/20
 * Time: 11.14
 */
class HelloResponseDtoTest {
    @Test
    public void createHelloResponseDto() {
        //given
        String name = "Naruto";
        int age = 23;

        //when
        HelloResponseDto helloResponseDto = new HelloResponseDto(name, age);

        //then
        assertEquals(helloResponseDto.getAge(), age);
        assertEquals(helloResponseDto.getName(), name);
    }
}