package com.hendisantika.springbootmustachesample.model;

import com.hendisantika.springbootmustachesample.model.post.Posts;
import com.hendisantika.springbootmustachesample.model.post.PostsRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/03/20
 * Time: 11.05
 */
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest
class BaseTimeEntityTest {
    @Autowired
    private PostsRepository postsRepository;

    @AfterEach
    public void tearDown() {
        postsRepository.deleteAll();
    }

    @Test
    public void test_CreatedTime_ModifiedTime() {
        //given
        LocalDateTime now = LocalDateTime.of(2021, 1, 10, 0, 0, 0);
        String title = "title";
        String content = "content";
        String author = "author";

        //when
        postsRepository.save(Posts.builder().title(title).content(content).author(author).build());
        //then
        List<Posts> postsList = postsRepository.findAll();
        System.out.println("test --> " + postsList.get(0).getCreatedDate().isBefore(now));
        assertTrue(postsList.get(0).getCreatedDate().isBefore(now));
    }
}