package com.hendisantika.springbootmustachesample.model.post;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/03/20
 * Time: 11.12
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class PostsRepositoryTest {
    @Autowired
    private PostsRepository postsRepository;

    @AfterEach
    public void tearDown() {
        postsRepository.deleteAll();
    }

    @Test
    public void testPostsRepository() {
        String title = "Lee Si Won";
        String content = "content1";
        String author = "siwon0417";

        postsRepository.save(Posts.builder()
                .title(title)
                .content(content)
                .author(author).build());

        List<Posts> postsList = postsRepository.findAll();

        assertEquals(postsList.get(0).getTitle(), title);
        assertEquals(postsList.get(0).getContent(), content);
        assertEquals(postsList.get(0).getAuthor(), author);
    }

}