package com.hendisantika.springbootmustachesample.web.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/03/20
 * Time: 10.19
 */

@Getter
@RequiredArgsConstructor
public class HelloResponseDto {
    @NonNull
    private final String name;

    @NonNull
    private final int age;
}