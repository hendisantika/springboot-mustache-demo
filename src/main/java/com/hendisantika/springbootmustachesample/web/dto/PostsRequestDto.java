package com.hendisantika.springbootmustachesample.web.dto;

import com.hendisantika.springbootmustachesample.model.post.Posts;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/03/20
 * Time: 07.19
 */
@Getter
@NoArgsConstructor
public class PostsRequestDto {

    private String title;
    private String content;
    private String author;

    @Builder
    public PostsRequestDto(String title, String content, String author) {
        this.title = title;
        this.content = content;
        this.author = author;
    }

    public Posts toEntity() {
        return Posts.builder().title(this.title).content(this.content).author(this.author).build();
    }
}