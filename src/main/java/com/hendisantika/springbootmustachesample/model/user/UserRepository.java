package com.hendisantika.springbootmustachesample.model.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/03/20
 * Time: 17.23
 */
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByEmail(String email);
}
