package com.hendisantika.springbootmustachesample.model.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/03/20
 * Time: 17.19
 */
@Getter
@RequiredArgsConstructor
public enum Role {

    GUEST("ROLE_GUEST","Guest"),
    USER("ROLE_USER","End user");

    private final String key;
    private final String title;
}