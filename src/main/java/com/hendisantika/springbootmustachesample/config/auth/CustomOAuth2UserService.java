package com.hendisantika.springbootmustachesample.config.auth;

import com.hendisantika.springbootmustachesample.config.auth.dto.OAuthAttributes;
import com.hendisantika.springbootmustachesample.config.auth.dto.SessionUser;
import com.hendisantika.springbootmustachesample.model.user.User;
import com.hendisantika.springbootmustachesample.model.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/03/20
 * Time: 10.12
 */
@RequiredArgsConstructor
@Service
public class CustomOAuth2UserService implements OAuth2UserService<OAuth2UserRequest, OAuth2User> {
    private final UserRepository userRepository;
    private final HttpSession httpSession;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2UserService delegate = new DefaultOAuth2UserService();
        OAuth2User oAuth2User = delegate.loadUser(userRequest);

        //Classification of currently logged-in service (google or naver or etc.)
        String registerationId = userRequest.getClientRegistration().getRegistrationId();
        //Field value that becomes the key when proceeding with OAuth2 login (Google's Default: sub)
        String userNameAttributeName =
                userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUserNameAttributeName();
        //Class to contain OAuth2User attribute
        OAuthAttributes attributes = OAuthAttributes.of(registerationId, userNameAttributeName,
                oAuth2User.getAttributes());
        User user = saveOrUpdate(attributes);
        //DTO to store user information in Session
        httpSession.setAttribute("user", new SessionUser(user));

        return new DefaultOAuth2User(
                Collections.singleton(new SimpleGrantedAuthority(user.getRoleKey())),
                attributes.getAttributes(),
                attributes.getNameAttributeKey());

    }

    private User saveOrUpdate(OAuthAttributes attributes) {
        User user = userRepository.findByEmail(attributes.getEmail())
                .map(entity -> entity.update(attributes.getName(), attributes.getPicture()))
                .orElse(attributes.toEntity());

        return userRepository.save(user);
    }
}
