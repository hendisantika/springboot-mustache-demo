package com.hendisantika.springbootmustachesample.config.auth.dto;

import com.hendisantika.springbootmustachesample.model.user.User;
import lombok.Getter;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/03/20
 * Time: 17.38
 */
@Getter
public class SessionUser implements Serializable {

    private String name;
    private String email;
    private String picture;

    public SessionUser(User user) {
        this.name = user.getName();
        this.email = user.getEmail();
        this.picture = user.getPicture();
    }
}
