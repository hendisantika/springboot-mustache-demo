package com.hendisantika.springbootmustachesample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMustacheSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMustacheSampleApplication.class, args);
    }

}
