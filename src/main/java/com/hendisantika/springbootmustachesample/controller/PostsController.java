package com.hendisantika.springbootmustachesample.controller;

import com.hendisantika.springbootmustachesample.service.PostService;
import com.hendisantika.springbootmustachesample.web.dto.PostsRequestDto;
import com.hendisantika.springbootmustachesample.web.dto.PostsResponseDto;
import com.hendisantika.springbootmustachesample.web.dto.PostsUpdateRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/03/20
 * Time: 08.50
 */
@RequiredArgsConstructor
@RestController
public class PostsController {

    private final PostService postService;

    //Create
    @PostMapping("/api/v1/posts")
    public Long save(@RequestBody PostsRequestDto requestDto) {
        return postService.save(requestDto);
    }

    //Read
    @GetMapping("/api/v1/posts/{id}")
    public PostsResponseDto findByID(@PathVariable Long id) {
        return postService.findById(id);
    }

    //Update
    @PutMapping("api/v1/posts/{id}")
    public Long update(@PathVariable Long id, @RequestBody PostsUpdateRequestDto requestDto) {
        return postService.update(id, requestDto);
    }

    //Delete
    @DeleteMapping("api/v1/posts/{id}")
    public Long delete(@PathVariable Long id) {
        postService.delete(id);
        return id;
    }
}
