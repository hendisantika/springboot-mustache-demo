package com.hendisantika.springbootmustachesample.controller;

import com.hendisantika.springbootmustachesample.config.auth.LoginUser;
import com.hendisantika.springbootmustachesample.config.auth.dto.SessionUser;
import com.hendisantika.springbootmustachesample.service.PostService;
import com.hendisantika.springbootmustachesample.web.dto.PostsResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpSession;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/03/20
 * Time: 08.48
 */
@Controller
@RequiredArgsConstructor
public class IndexController {

    private final HttpSession httpSession;
    private final PostService postService;

    @GetMapping("/")
    public String index(Model model, @LoginUser SessionUser user) {
        if (user != null) {
            model.addAttribute("userName", user.getName());
        }
        model.addAttribute("posts", postService.findAllDesc());
        return "index";
    }

    @GetMapping("posts/save")
    public String postsSave() {
        return "posts-save";
    }

    @GetMapping("posts/update/{id}")
    public String postsUpdate(@PathVariable Long id, Model model) {
        PostsResponseDto responseDto = postService.findById(id);
        model.addAttribute("post", responseDto);
        return "posts-update";
    }
}
