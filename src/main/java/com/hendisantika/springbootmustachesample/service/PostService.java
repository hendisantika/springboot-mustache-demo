package com.hendisantika.springbootmustachesample.service;

import com.hendisantika.springbootmustachesample.model.post.Posts;
import com.hendisantika.springbootmustachesample.model.post.PostsRepository;
import com.hendisantika.springbootmustachesample.web.dto.PostsListResponseDto;
import com.hendisantika.springbootmustachesample.web.dto.PostsRequestDto;
import com.hendisantika.springbootmustachesample.web.dto.PostsResponseDto;
import com.hendisantika.springbootmustachesample.web.dto.PostsUpdateRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mustache-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/03/20
 * Time: 07.21
 */
@RequiredArgsConstructor
@Service
public class PostService {

    private final PostsRepository postsRepository;

    @Transactional
    public Long save(PostsRequestDto requestDto) {
        return postsRepository.save(requestDto.toEntity()).getId();
    }

    public PostsResponseDto findById(Long id) {
        Posts entity = postsRepository.findById(id)
                .orElseThrow(() -> new
                        IllegalArgumentException("User X. id = " + id));
        return new PostsResponseDto(entity);
    }

    @Transactional
    public Long update(Long id, PostsUpdateRequestDto requestDto) {
        Posts entity = postsRepository.findById(id)
                .orElseThrow(() -> new
                        IllegalArgumentException(("User X. id = " + id)));

        entity.update(requestDto.getTitle(), requestDto.getContent());
        return id;
    }

    @Transactional(readOnly = true)
    public List<PostsListResponseDto> findAllDesc() {
        return postsRepository.findAllDesc().stream()
                .map(PostsListResponseDto::new)
                .collect(Collectors.toList());
    }

    public Long delete(Long id) {
        postsRepository.deleteById(id);
        return id;
    }
}
